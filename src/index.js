import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';

import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import rootReducer from './redux/reducers';

// Import Middlewares
import logger from 'redux-logger';
import ReduxThunk from 'redux-thunk';

let store = createStore(rootReducer, applyMiddleware(logger, ReduxThunk))

ReactDOM.render(
    <Provider store={store}>
    <App />
    </Provider> 
    , document.getElementById('root'));

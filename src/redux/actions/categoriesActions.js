import axios from 'axios';

// GET PRODUCTS
export function getCategories() {
    return function (dispatch) {
        axios.get("http://localhost:8081/api/categories")
            .then(function (response) {
                dispatch({ type: "GET_CATEGORIES", payload: response.data })
            })
            .catch(function (err) {
                dispatch({ type: "GET_CATEGORIES_REJECTED", payload: err })
            })
    }
}

import axios from 'axios';

// GET PRODUCTS
export function getReviews() {
    return function (dispatch) {
        axios.get("http://localhost:8081/api/reviews")
            .then(function (response) {
                dispatch({ type: "GET_REVIEWS", payload: response.data })
            })
            .catch(function (err) {
                dispatch({ type: "GET_REVIEWS_REJECTED", payload: err })
            })
    }
}

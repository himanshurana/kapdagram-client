import axios from 'axios';
// GET CART 

function getAjaxObject() {

    return axios.create({
        withCredentials: true,
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json; charset=utf-8',
            'Access-Control-Allow-Origin': '*'
        },
    });
}

export function getCart() {
    return function (dispatch) {
        const ajax = getAjaxObject();
        ajax.get('http://localhost:8081/cart')
            .then(function (response) {
                console.log(response.data);
                dispatch({
                    type: "GET_CART",
                    payload: response.data
                })
            })
            .catch(function (err) {
                console.log('err', err)
                dispatch({ type: "GET_CART_REJECTED", msg: "error when getting the cart from session" })
            });
    }
}
// ADD TO CART
export function addToCart(productId) {
    let URL = `http://localhost:8081/add-to-cart/${productId}`;
    return function (dispatch) {
        const ajax = getAjaxObject();
        ajax.post(URL)
            .then(function (response) {
                dispatch({
                    type: "ADD_TO_CART",
                    payload: response.data
                })
            }).catch(function (err) {
                dispatch({ type: "ADD_TO_CART_REJECTED", msg: 'error when adding to the cart' })
            })
    }
}

// REDUCE ITEM QUANTITY
export function reduce(productId) {
    let URL = `http://localhost:8081/reduce/${productId}`;
    return function (dispatch) {
        const ajax = getAjaxObject();
        ajax.get(URL)
            .then(function (response) {
                console.log(response)
                dispatch({
                    type: "REDUCE_CART",
                    payload: response.data
                })
            }).catch(function (err) {
                dispatch({ type: "REDUCE_REJECTED", msg: 'error when adding to the cart' })
            })
    }
}
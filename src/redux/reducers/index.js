import { combineReducers } from 'redux'

//HERE IMPORT REDUCERS TO BE COMBINED
import { cartReducers } from './cartReducer';
import { categories } from './categoriesReducers';
import { products } from './ProductsReducer';
import { reviews } from './reviewReducers';

export default combineReducers({
    Cart: cartReducers,
    products,
    categories,
    reviews
});
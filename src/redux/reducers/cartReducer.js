//CART REDUCERS
export function cartReducers(state = { cart: [] }, action) {
    console.log(action)
    switch (action.type) {
        case "GET_CART":
            return {
                ...state,
                cart: action.payload
            }
            break;
        case "ADD_TO_CART":
            return {
                ...state,
                cart: action.payload
            }
            break;
        case "UPDATE_CART":
            return {
                ...state,
                cart: action.payload
            }
            break;
        case "DELETE_CART_ITEM":
            return {
                ...state,
                cart: action.payload
            }
            break;
    }
    return state
}

// // CALCULATE TOTALS 
// export function totals(payloadArr) {
//     const totalAmount = payloadArr.map(function (cartArr) {
//         return cartArr.price * cartArr.quantity;
//     }).reduce(function (a, b) {
//         return a + b;
//     }, 0); //start summing from index0

//     const totalQty = payloadArr.map(function (qty) {
//         return qty.quantity;
//     }).reduce(function (a, b) {
//         return a + b;
//     }, 0);
//     return { amount: totalAmount.toFixed(2), qty: totalQty }
// }

// need to know why this statment is not working "...state" and this working "...state.cart" 
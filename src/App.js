import React, { Component } from 'react';
// import './App.css';
import { Route, Link, BrowserRouter as Router } from 'react-router-dom';

// Import Components
import Header from './components/common/Header';
import Landing from './components/Landing';
import Products from './components/Products';
import Cart from './components/Cart';
import Product from './components/Product';
import ProductForm from './components/Admin/ProductsForm';
import Address from './components/Address';
import Summery from './components/Summery';
import Payment from './components/Payment';
import Orders from './components/Orders';
import LoginForm from './components/LoginForm';

class App extends Component {
  render() {
    return (
      <Router>
        <div>
          <Header />
          {/* <Route exact path="/" component={Header} /> */}
          <Route exact path="/" component={Landing} />
          <Route exact path="/login" component={LoginForm} />
          <Route exact path="/products" component={Products} />
          <Route exact path="/cart" component={Cart} />
          <Route exact path="/checkout_address" component={Address} />
          <Route exact path="/checkout_summery" component={Summery} />
          <Route exact path="/checkout_payment" component={Payment} />
          <Route exact path="/orders" component={Orders} />
          <Route exact path="/product" component={Product} />
          <Route exact path="/admin/product_form" component={ProductForm} />
        </div>
      </Router>
    );
  }
}

export default App;

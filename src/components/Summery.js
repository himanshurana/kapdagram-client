import React, { Component } from 'react';
import {Link} from 'react-router-dom'

class Summery extends Component {
    render() {
        return (
            <div className="row">
                <div className="col-md-1"></div>
                <div className="col-lg-7" style={{marginTop: '30px'}}>
                    <div style={{ backgroundColor: '#c2d7f9', height: '600px', textAlign: 'center', boxShadow: '2px 2px 10px lightgray' }}>Order Summery</div>
                </div>
                <div className="col-lg-3" >
                    <div style={{
                        display: 'flex', flexDirection: 'column', justifyContent: 'space-around',
                        height: '400px', textAlign: 'center', boxShadow: '2px 2px 10px lightgray',
                        backgroundColor: '#f4f5f7', padding: '10px', marginTop: '30px' 
                    }}>
                        <h4>Order Summery</h4>
                        <div style={{ display: 'flex', flexDirection: 'column', justifyContent: 'space-around', height: '200px' }}>
                            <div style={{ display: 'flex', justifyContent: 'space-between', width: '100%' }}>
                                <div className="">Cart Total</div>
                                <div className="">Rs. 1500</div>
                            </div>
                            <div style={{ display: 'flex', justifyContent: 'space-between', width: '100%' }}>
                                <div className="">Shipping Charges</div>
                                <div className="">FREE</div>
                            </div>
                            <div style={{ display: 'flex', justifyContent: 'space-between', width: '100%' }}>
                                <div className="">Discount</div>
                                <div className="">Rs. -100</div>
                            </div>
                            <div style={{ display: 'flex', justifyContent: 'space-between', width: '100%' }}>
                                <div className="">Payable Amount</div>
                                <div className="">Rs. 1500</div>
                            </div>
                        </div>
                    <Link to="checkout_payment">
                        <button className="btn btn-primary">Payment</button>
                    </Link>
                    </div>
                    <div style={{
                        display: 'flex', flexDirection: 'column', justifyContent: 'space-around',
                        height: '150px', boxShadow: '2px 2px 10px lightgray',
                        backgroundColor: '#f4f5f7', margin: '20px 0', padding: '10px'
                    }}>
                        <small>Delivery Address</small>
                        <div style={{ display: 'flex', flexDirection: 'column', justifyContent: 'space-around' }}>
                            <small>Name</small>
                            <small>Full Address</small>
                            <small>Mobile Number</small>
                        </div>
                        <p style={{ color: '#67CECD', fontSize: '1em' }}>Change</p>
                    </div>
                </div>
                <div className="col-ms-1"></div>
            </div>
        );
    }
}

export default Summery;

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Well, Panel, Col, Row, Button, InputGroup, FormControl, DropdownButton, FormGroup, ControlLabel, HelpBlock, Image, imgList, ButtonGroup, Label } from 'react-bootstrap';
import { findDOMNode } from 'react-dom';
//IMPORT ACTIONS
import { bindActionCreators } from 'redux';
import { getProducts, postProduct, getCategories } from '../../redux/actions/adminActions';

class ProductForm extends Component {

    componentDidMount() {
        this.props.getProducts();
        this.props.getCategories();
    }

    categoriesList = (categories) => {
        return categories.categories.map(categorie => {
            return <option value={categorie._id}>{categorie.categorie}</option>
        })
    }

    handleSubmit() {
        const product = {
            title: findDOMNode(this.refs.title).value,
            description: findDOMNode(this.refs.description).value,
            // images: findDOMNode(this.refs.image).value,
            prices: {
                sell_price: findDOMNode(this.refs.sell_price).value,
                mrp: findDOMNode(this.refs.mrp).value,
                discount: findDOMNode(this.refs.discount).value,
            },
            categorie: { _id: findDOMNode(this.refs.categorie).value }
        };
        console.log(product)
        // CART IS EMPTY
        this.props.postProduct(product);

    }

    render() {
        return (
            <Well>
                <Row>
                    <Col xs={6}>
                        <Panel>
                            <InputGroup>
                                <FormControl
                                    id="formControlsFile"
                                    type="file"
                                    label="File"

                                />
                                {"Example block-level help text here." &
                                    <HelpBlock ref="image">Example block-level help text here.</HelpBlock>
                                }
                            </InputGroup>
                        </Panel>
                    </Col>
                    <Col xs={12}>
                        <Panel>
                            <FormGroup controlId="title">
                                <ControlLabel>Title</ControlLabel>
                                <FormControl type="text" placeholder="Enter Title" ref="title" />
                            </FormGroup>
                            <FormGroup controlId="description">
                                <ControlLabel>Description</ControlLabel>
                                <FormControl type="text" placeholder="Enter Description" ref="description" />
                            </FormGroup>
                            <Row>
                                <Col xs={4}>
                                    <FormGroup controlId="price" >
                                        <ControlLabel>Selling Price</ControlLabel>
                                        <FormControl type="text" placeholder="Enter Selling Price" ref="sell_price" />
                                    </FormGroup>
                                </Col>
                                <Col xs={4}>
                                    <FormGroup controlId="price">
                                        <ControlLabel>MRP</ControlLabel>
                                        <FormControl type="text" placeholder="Enter MRP" ref="mrp" />
                                    </FormGroup>
                                </Col>
                                <Col xs={4}>
                                    <FormGroup controlId="price">
                                        <ControlLabel>Discount</ControlLabel>
                                        <FormControl type="text" placeholder="Enter Discount" ref="discount" />
                                    </FormGroup>
                                </Col>
                            </Row>
                        </Panel>
                        <Panel>
                            <FormGroup controlId="formControlsSelect">
                                <ControlLabel>Categorie</ControlLabel>
                                <FormControl ref="categorie" componentClass="select" placeholder="select">
                                    <option value="select">select</option>
                                    {this.categoriesList(this.props.categories)}
                                </FormControl>
                            </FormGroup>
                        </Panel>
                        <Button onClick={this.handleSubmit.bind(this)} bsStyle="primary">Save book</Button>
                    </Col>
                </Row>
            </Well >
        )
    }
}

function mapStateToProps({ products, categories }) {
    return { products, categories }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        getProducts,
        postProduct,
        getCategories
    },
        dispatch)
}


export default connect(mapStateToProps, mapDispatchToProps)(ProductForm);

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Well, Panel, Col, Row, Button, ButtonGroup, Label } from 'react-bootstrap';
//IMPORT ACTIONS
import { getCart, reduce, addToCart } from '../redux/actions/cartActions';
import { bindActionCreators } from 'redux';

import axios from 'axios';

class Cart extends Component {
    constructor(props) {
        super(props);

        this.state = {
            cart: {}
        }
    }
    componentDidMount() {
        this.props.getCart();
    }

    renderEmpty = () => {
        return <div>Empty this section!</div>;
    }

    componentWillReceiveProps(nextProps) {
        this.setState({ cart: nextProps.cart.cart })
        console.log('nextProps', nextProps)
    }

    onDelete = (_id) => {
        console.log(_id)
        // Create a copy of the current array of books
        const currentBookToDelete = this.props.cart.cart;
        // Determine at which index in books array is the book to be deleted
        const indexToDelete = currentBookToDelete.findIndex(
            function (cart) {
                return cart._id === _id;
            }
        )
        // use slice to remove the book at the specified index
        let cartAfterDelete = [...currentBookToDelete.slice(0, indexToDelete), ...currentBookToDelete.slice(indexToDelete + 1)];

        this.props.deleteCartItem(cartAfterDelete);
    }

    renderCart = () => {
        const cart = this.state.cart;
        const cartItemsList = cart.products.map((cartArr) => {
            console.log('cartArr', cartArr.item._id)
            return (
                <Row>
                    <Col xs={12} sm={4}>
                    <img src={cartArr.item.img} alt="Smiley face" height="100" width="100" />
                        <small>title</small>
                        <h6>{cartArr.item.title}</h6><span>    </span>
                    </Col>
                    <Col xs={12} sm={2}>
                        <small>price</small>
                        <h6>{cartArr.price}</h6><span>    </span>
                    </Col>
                    <Col xs={12} sm={2}>
                        <small>qty</small>
                        <h6>{cartArr.qty} <Label bsStyle="success"></Label></h6>
                    </Col>
                    <Col xs={6} sm={4}>
                        <ButtonGroup style={{ minWidth: '360' }} >
                            <Button onClick={() => this.props.reduce(cartArr.item._id)} bsStyle="default" bsSize="small">-</Button>
                            <Button onClick={()=> {this.props.addToCart(cartArr.item._id)}} bsStyle="default" bsSize="small">+</Button>
                            <span></span>
                            <Button onClick={() => { this.onDelete(cartArr.item._id) }} bsStyle="danger" bsSize="small">DELETE</Button>
                        </ButtonGroup>
                    </Col>
                </Row>
            )
        })
        return (
            <Panel header="Cart" bsStyle="primary" >
                <Row>
                    <Col md={1}></Col>
                    <Col md={7}>
                        <div style={{ backgroundColor: '#c2d7f9', height: '600px', textAlign: 'center', boxShadow: '2px 2px 10px lightgray' }}>
                            {cartItemsList}
                        </div>
                    </Col>
                    <Col md={3} style={{
                        display: 'flex', flexDirection: 'column', justifyContent: 'space-around',
                        height: '400px', textAlign: 'center', boxShadow: '2px 2px 10px lightgray',
                        backgroundColor: '#f4f5f7',
                    }}>
                        <h4>Order Summery</h4>
                        <div style={{ display: 'flex', flexDirection: 'column', justifyContent: 'space-around', height: '200px' }}>
                            <div style={{ display: 'flex', justifyContent: 'space-between', width: '100%' }}>
                                <div className="">Cart Total</div>
                                <div className="">Rs.{cart.totalPrice}</div>
                            </div>
                            <div style={{ display: 'flex', justifyContent: 'space-between', width: '100%' }}>
                                <div className="">Shipping Charges</div>
                                <div className="">FREE</div>
                            </div>
                            <div style={{ display: 'flex', justifyContent: 'space-between', width: '100%' }}>
                                <div className="">Payable Amount</div>
                                <div className="">Rs. {cart.totalPrice}</div>
                            </div>
                        </div>
                        <Link to="/checkout_address">
                            <button className="btn btn-primary">CHECKOUT</button>
                        </Link>
                    </Col>
                    <Col md={1}></Col>
                </Row>
            </Panel >
        )
    }


    render() {
        if (this.props.cart.cart.products !== undefined) {
            return this.renderCart();
        } else {
            return this.renderEmpty();
        }
    }
}

function mapStateToProps(state) {
    return { cart: state.Cart }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        getCart,
        reduce,
        addToCart
    },
        dispatch)
}


export default connect(mapStateToProps, mapDispatchToProps)(Cart);
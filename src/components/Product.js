import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { addToCart } from '../redux/actions/cartActions';
import { Link } from 'react-router-dom';
import Review from './Review';

class Proudct extends Component {

    render() {
        const selectedProduct = JSON.parse(localStorage.getItem('selectedProduct'));;

        console.log(selectedProduct)
        return (
            <div className="row" style={{ marginTop: '50px' }}>
                <div className="col-md-1"></div>
                <div className="col-md-3">
                    <div style={{ border: '1px solid gray' }}>
                        <img src={selectedProduct.img} alt="Smiley face" className="img-fluid" max-width="100%" heigh="auto" />
                    </div>
                </div>
                <div className="col-md-6" style={{}}>
                    <div style={{}}>
                        <p>{selectedProduct.title}</p>
                        <h4>₹  {selectedProduct.prices.sell_price}</h4>
                        <div>
                            <p>Color</p>
                            <div style={{display:"flex"}}>
                                <div style={{width: 30, height: 30, margin: '0 10px 0 0', backgroundColor: "blue" }}></div>
                                <div style={{width: 30, height: 30, margin: '0 10px 0 0', backgroundColor: "black" }}></div>
                                <div style={{width: 30, height: 30, margin: '0 10px 0 0', backgroundColor: "gray" }}></div>
                            </div>
                        </div>
                        <div style={{display:'flex'}}>
                            <h6>Size</h6>
                            <p>S</p>
                            <p>M</p>
                            <p>L</p>
                            <p>XL</p>
                            <p>XXL</p>
                        </div>
                    </div>
                    <Link to="/cart">
                        <button
                            className="btn"
                            style={{ backgroundColor: "#51CCCC", color: "white" }}
                            onClick={() => this.props.addToCart(selectedProduct._id)}

                        >ADD TO CART</button>
                    </Link>
                    <Review product={selectedProduct} />
                </div>
                <div className="col-ms-1"></div>
            </div>
        );
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ addToCart }, dispatch)
}

export default connect(null, mapDispatchToProps)(Proudct);

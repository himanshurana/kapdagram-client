import React, { PureComponent } from 'react';
import { Link } from 'react-router-dom';

const handleOnProductClick = (product) => {
    localStorage.setItem('selectedProduct', JSON.stringify(product));
}

const ProductsListItem = (props) => {
    return props.fetchedProducts.map(product => {
        return (
            <Link
                className="col-sm-6 col-lg-4"
                onClick={() => handleOnProductClick(product)}
                to="/product"
            >
                <div style={{ margin: '10px 0' }}>
                    <img src={product.img} alt="product image" className="img-thumbnail" />
                    <p>{product.title}</p>
                    <div style={{ display: 'flex', flex: 1, justifyContent: 'space-around' }}>
                        <p>Price:</p>
                        <p>{product.prices.sell_price}</p>
                    </div>
                </div>
            </Link>)
    })
}

export default ProductsListItem;

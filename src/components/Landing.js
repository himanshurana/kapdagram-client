import React, { Component } from 'react';
import '../CSS/landing.css';

class Landing extends Component {
    render() {
        return (
            <div className="container">
                <div style={{ margin: "50px 0", textAlign: 'center' }}>
                    <h4>S T A F F  P I C K</h4>
                    <div className="row">
                        <div className="col-sm-6 col-lg-3">
                            <div style={{ backgroundColor: 'pink', height: '200px', margin: '10px 0' }}></div>
                        </div>
                        <div className="col-sm-6 col-lg-3" >
                            <div style={{ backgroundColor: 'pink', height: '200px', margin: '10px 0' }}></div>
                        </div>
                        <div className="col-sm-6 col-lg-3" >
                            <div style={{ backgroundColor: 'pink', height: '200px', margin: '10px 0' }}></div>
                        </div>
                        <div className="col-sm-6 col-lg-3" >
                            <div style={{ backgroundColor: 'pink', height: '200px', margin: '10px 0' }}></div>
                        </div>
                    </div>
                </div>
                <div style={{ margin: "50px 0", textAlign: 'center' }}>
                    <h4>T R E N D I N G</h4>
                    <div className="row">
                        <div className="col-lg-6" >
                            <div style={{ backgroundColor: 'skyblue', height: '200px', margin: '10px 0' }}></div>
                        </div>
                        <div className="col-lg-6" >
                            <div style={{ backgroundColor: 'skyblue', height: '200px', margin: '10px 0' }}></div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Landing;

"use strict"
import React, { Component } from 'react';
import { Well, Panel, Col, Row, Button, ButtonGroup, Label } from 'react-bootstrap';
import { connect } from 'react-redux';
import { deleteCartItem, updateCart, getCart } from '../../redux/actions/cartActions';

import { bindActionCreators } from 'redux';

class Cart extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showModal: false
        }
    }

    componentDidMount() {
        // console.log(this.props.getCart )
        this.props.getCart();
    }

    open() {
        this.setState({ showModal: true })
    }
    close() {
        this.setState({ showModal: false })
    }
    renderEmpty = () => {
        return <div>Empty</div>;
    }


    onDelete = (_id) => {
        console.log(_id)
        // Create a copy of the current array of books
        const currentBookToDelete = this.props.cart;
        // Determine at which index in books array is the book to be deleted
        const indexToDelete = currentBookToDelete.findIndex(
            function (cart) {
                return cart._id === _id;
            }
        )
        console.log(currentBookToDelete)
        console.log(indexToDelete)
        // use slice to remove the book at the specified index
        let cartAfterDelete = [...currentBookToDelete.slice(0, indexToDelete), ...currentBookToDelete.slice(indexToDelete + 1)];

        this.props.deleteCartItem(cartAfterDelete);
    }
    onIncrement(_id) {
        this.props.updateCart(_id, 1, this.props.cart);
    }
    onDecrement(_id, quantity) {
        if (quantity > 1) {
            this.props.updateCart(_id, -1, this.props.cart);
        }
    }

    renderCart = () => {
        const cartItemsList = this.props.cart.map(cartArr => {
            return (
                <Panel key={cartArr._id}>
                    <Row>
                        <Col xs={12} sm={4}>
                            <h6>{cartArr.title}</h6><span>    </span>
                        </Col>
                        <Col xs={12} sm={2}>
                            <h6>{cartArr.price}</h6><span>    </span>
                        </Col>
                        <Col xs={12} sm={2}>
                            <h6>qty. <Label bsStyle="success"></Label></h6>
                        </Col>
                        <Col xs={6} sm={4}>
                            <ButtonGroup style={{ minWidth: '360' }} >
                                <Button onClick={this.onDecrement.bind(this, cartArr._id, cartArr.quantity)} bsStyle="default" bsSize="small">-</Button>
                                <Button onClick={this.onDecrement.bind(this, cartArr._id, cartArr.quantity)} bsStyle="default" bsSize="small">-</Button>
                                <span></span>
                                <Button onClick={() => { this.onDelete(cartArr._id) }} bsStyle="danger" bsSize="small">DELETE</Button>
                            </ButtonGroup>
                        </Col>
                    </Row>
                </Panel>
            )
        })
        return (
            <Panel header="Cart" bsStyle="primary">
                {cartItemsList}
            </Panel>
        )
    }

    render() {
        if (this.props.cart[0]) {
            return this.renderCart();
        } else {
            return this.renderEmpty();
        }
    }
}

function mapStateToProps(state) {
    return { cart: state.Cart.cart }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        getCart: getCart,
        deleteCartItem:deleteCartItem

    },
        dispatch)
}

export default connect(mapStateToProps,  mapDispatchToProps)(Cart);
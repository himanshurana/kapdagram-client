"use strict"
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getBooks } from '../../redux/actions/booksActions';
import { Grid, Col, Row, Button } from 'react-bootstrap';

import BookItem from './bookItem';
import booksForm from './booksForm';
import BookForm from './booksForm';
import Cart from './Cart';

class BooksList extends Component {

    componentDidMount() {
        // Dispatch an action
        this.props.getBooks();
    }

    render() {
        const booksList = this.props.books.map(booksArr => {
            return (
                <Col xs={12} sm={6} md={4} key={booksArr._id} >
                    <BookItem
                        _id={booksArr._id}
                        title={booksArr.title}
                        description={booksArr.description}
                        price={booksArr.price}
                    />
                </Col>
            )
        })
        return (
            <Grid>
                <Row>
                    <Col xs={12} sm={6}>
                        <BookForm />
                    </Col>
                    {booksList}
                </Row>
            </Grid>
        )
    }
}

function mapStateToProps(state) {
    return {books: state.Books.books};
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        getBooks: getBooks
    },
        dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(BooksList);
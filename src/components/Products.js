// "use strict"
// import React, { Component } from 'react';
// import { connect } from 'react-redux';
// import { bindActionCreators } from 'redux';

// import { Grid, Col, Row, Button } from 'react-bootstrap';

// import ProductItem from './common/productItem';
// // import productsForm from './productsForm';
// // import productForm from './productsForm';
// // import Cart from './Cart';

// class ProductsList extends Component {

//     componentDidMount() {
//         // Dispatch an action
//         this.props.getProducts();
//     }

//     render() {
//         const productsList = this.props.products.map(productsArr => {
//             return (
//                 <Col xs={12} sm={6} md={4} key={productsArr._id} >
//                     <ProductItem
//                         _id={productsArr._id}
//                         title={productsArr.title}
//                         description={productsArr.description}
//                         price={productsArr.price}
//                     />
//                 </Col>
//             )
//         })
//         return (
//             <Grid>
//                 <Row>
//                     <Col xs={12} sm={6}>
//                         <productForm />
//                     </Col>
//                     {productsList}
//                 </Row>
//             </Grid>
//         )
//     }
// }



// export default connect(mapStateToProps, mapDispatchToProps)(ProductsList);







import React, { Component } from 'react';
import { connect } from 'react-redux';
// REACT BOOTSTRAP
import { Grid, Col, Row, Button } from 'react-bootstrap';
// REDUX
import { bindActionCreators } from 'redux';
import { getCategories } from '../redux/actions/categoriesActions';
import { getProducts } from '../redux/actions/adminActions';
// COMPONENT
import ProductsListItem from './common/ProductsListItem';//Actions
// CSS
import '../CSS/productsPage.css'

class Proudcts extends Component {
    constructor(props) {
        super(props);

        this.state = {
            products: [],
            fetchedCategories: [],
            filteredProducts: []
        }
    }

    componentDidMount() {
        this.props.getProducts();
        this.props.getCategories();
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            products: nextProps.products.products,
            filteredProducts: nextProps.products.products,
            fetchedCategories: nextProps.categories.categories
        })
    }

    filterCategories = (categorie) => {
        // use filterProducts variable for saving the filtered products
        let filterProducts = []
        this.state.products.map(product => {
            product.categorie.map(id => {
                // make sure prduct has categorie id
                if(id !== undefined){
                    //campare the product categorie to categorie
                    if(id === categorie._id){
                        filterProducts.push(product)
                        // update the filteredProducts state variable
                        this.setState({filteredProducts: filterProducts})
                    }
                }
            })
        })
    }

    fetchedCategories = () => {
        return this.state.fetchedCategories.map(categorie => {
            return <p onClick={() => this.filterCategories(categorie)} className="categorie">{categorie.categorie}</p>
        })
    }

    render() {
        return (
            <div className="container">
                <div>
                    <div id="categories">{this.fetchedCategories()}</div>
                </div>
                <div>

                </div>
                <Grid>
                    <h4>Products</h4>
                    <Row>
                        <ProductsListItem fetchedProducts={this.state.filteredProducts} />
                    </Row>
                </Grid>
                {/* <div className="col-md-8" style={{ textAlign: 'center' }}>
                    
                    <div className="row">
                        
                    </div>
                </div> */}
            </div>
        );
    }
}

function mapStateToProps({ products, categories }) {
    return { products, categories };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        getProducts,
        getCategories
    }, dispatch)
}


export default connect(mapStateToProps, mapDispatchToProps)(Proudcts);
